Getting started 
===============

How to set up a clean install and install necessary commands


Installing HTSeq
HTseq is silly and needs to be installed manually with ``pip install HTSeq==<version num>``
*after* ``pip install -r requirements.txt`` is run. It requires that numpy is *already* 
installed and importable as a module, or it will throw an error. 
