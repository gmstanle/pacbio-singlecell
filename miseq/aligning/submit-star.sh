#!/bin/bash
#SBATCH --partition=normal,owners,quake
#SBATCH --job-name=star-miseq
#
#SBATCH --time=2:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64G

srun run-star.sh

