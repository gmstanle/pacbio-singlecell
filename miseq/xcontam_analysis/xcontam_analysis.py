# Script to quantify any mapping differences between the correctly- vs. incorrectly-mapping PacBio barcodes
# 190531 Geoff Stanley, Quake Lab

import pandas as pd
import numpy as np
import glob


bc_counts_all = pd.DataFrame(columns=['count','cell','bc_well'])
#print(bc_counts)

CHIMERA_DIR = '../../../../../storage/pacbio-singlecell/190502_bcPCRv2_TuPa23_exp1/'
chimera_files = glob.glob(CHIMERA_DIR + '*Chimeric.out.junction')
# print(chimera_files)

# iterate through chimeric junction files (1 file = 1 demuxed Nextera barcode)
for f in chimera_files:
    junctions = pd.read_csv(f,header=None,  sep='\t')
    cell_name = "_".join(f.split('/')[-1].split('_')[0:4])
    expected_barcode = cell_name.split("_")[1]
     
    # print(junctions[1].head())
    bc_regexp = "CS[12]_bc10"

    # get the indexes of the junctions containing the PB barcode on either one side or another
    mask_0 = junctions[0].str.contains(bc_regexp) # reads that map to barcodes on left side
    mask_1 = junctions[3].str.contains(bc_regexp) # reads that map to barcodes on right side


    if(np.sum(mask_0) + np.sum(mask_1) == 0 ): # if there are no PB barcode-mapping reads
        bc_counts = pd.DataFrame({'count': [0], 'cell': [cell_name], 'bc_well': [expected_barcode]})
    else:
        barcodes_0  = junctions[mask_0] 
        barcodes_1  = junctions[mask_1]

        # get the "well" identifier of the PacBio barcode - i.e., which well of the 96-well primer plate did it come from
        if np.sum(mask_0)==0:
            bc_wells =  barcodes_1[3].str.split("_", expand=True)[0].tolist()
            # barcodes_0.loc[:,'bc_well'] = bc_wells
            barcodes_1 = barcodes_1.assign(bc_well = bc_wells)
        elif np.sum(mask_1)==0:
            bc_wells =  barcodes_0[0].str.split("_", expand=True)[0].tolist() 
            # barcodes_0.loc[:,'bc_well'] = bc_wells
            barcodes_0 = barcodes_0.assign(bc_well = bc_wells)

        else:
            bc_wells_0 =  barcodes_0[0].str.split("_", expand=True)[0].tolist()
            bc_wells_1 = barcodes_1[3].str.split("_", expand=True)[0].tolist()
            # barcodes_0['bc_well'] = bc_wells_0
            # barcodes_1['bc_well'] = bc_wells_1
            # barcodes_0.loc[:,'bc_well'] = bc_wells_0
            # barcodes_1.loc[:,'bc_well'] = bc_wells_1
            barcodes_1 = barcodes_1.assign(bc_well = bc_wells_1)
            barcodes_0 = barcodes_0.assign(bc_well = bc_wells_0)

            bc_wells = bc_wells_0 + bc_wells_1

            print(barcodes_0.shape)
            print(barcodes_1.shape)

            # Count the number of reads per barcode
            bc_counts = pd.DataFrame(np.unique(np.asarray(bc_wells), return_counts=True))
            bc_counts.columns = bc_counts.iloc[0].tolist()
            bc_counts = pd.DataFrame(bc_counts.iloc[1])
            bc_counts.columns = ['count']
            # print(bc_counts)

            # Add column names and drop the index
            bc_counts['cell'] = cell_name
            bc_counts['bc_well'] = bc_counts.index
            bc_counts.reset_index(drop=True, inplace=True)
            # print(bc_counts)

            # Get reads aligning to expected vs wrong barcode
            # compute number of reads aligning to correct and incorrect barcodes
            # Output two files, one containing correct and one containing incorrect reads per cell
            # TODO compute matching length statistics
            # TODO compute map position statistics
            correct_0 = barcodes_0[barcodes_0['bc_well'] == expected_barcode] 
            correct_1 = barcodes_1[barcodes_1['bc_well'] == expected_barcode]
            correct_reads = correct_0.append(correct_1)
            correct_reads.to_csv(CHIMERA_DIR + cell_name + '_correct_bc_reads.tsv', sep='\t')

            correct_bc_counts = bc_counts[bc_counts['bc_well'] == expected_barcode]
            n_correct = correct_bc_counts['count']

            # reads to a barcode other than the expected PB barcode
            if(np.sum(bc_counts['bc_well'] != expected_barcode) > 0):
                incorrect_bc_counts = bc_counts[bc_counts['bc_well'] != expected_barcode]
                n_incorrect = np.sum(incorrect_bc_counts['count'])

                incorrect_0 = barcodes_0[barcodes_0['bc_well'] != expected_barcode] 
                incorrect_1 = barcodes_1[barcodes_1['bc_well'] != expected_barcode]
                incorrect_reads = incorrect_0.append(incorrect_1)
                incorrect_reads.to_csv(CHIMERA_DIR + cell_name + '_incorrect_bc_reads.tsv', sep='\t')
            else:
                n_incorrect = 0
            

        
        




        # print(barcodes_1.head())
        # print(barcodes_1['bc_well'].head())

        # print(barcodes_0.tail())
      
        # barcodes_1[3].str.split("_")[3].tolist()
        # print(bc_wells[1:10])
        # print(np.sum(mask_0) + np.sum(mask_1))
        # print(len(bc_wells))


        

        

    
