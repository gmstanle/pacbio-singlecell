import pandas as pd

# Make metadata for cells

data=pd.read_csv('../../data/pacbio_exp1.ccs.raw_data.csv')
nReads_cell = pd.DataFrame(data.groupby('bc_id').size())
nReads_cell.columns=['nReads']             
nIsoforms_cell = pd.DataFrame(data.loc[:,['bc_id','pbid']].drop_duplicates().groupby('bc_id').size())
nIsoforms_cell.columns=['nIsoforms']        
qc_cell = nReads_cell.merge(nIsoforms_cell, left_index=True, right_index=True)


meta_shortread = pd.read_csv('../../metadata/meta_table_shortread_fromMichelle.csv', index_col=1)
meta_shortread=meta_shortread.iloc[:,1:11]
meta_shortread=meta_shortread.rename(columns={x: x + '_shortRead' for x in meta_shortread.columns[6:]})

meta_pacbio_library = pd.read_csv('./metadata/hit picking - finalCellList_plateOrder.csv', index_col=0)
meta_pacbio_library = meta_pacbio_library[['plate','Well','Dest well']]
meta_pacbio_library.rename(columns={'plate': 'plate_shortRead','Well': 'well_shortRead','Dest well': 'well_pacbio'}, inplace=True) 
meta_pacbio_library=meta_pacbio_library.reset_index()

well2bc = pd.read_csv('../../metadata/pacbio_barcodes - pb_well2barcode.csv')
well2bc.rename(columns={'pacbio_well': 'well_pacbio','pacbio_bc': 'bc_id'}, inplace=True)

meta_pacbio_library = meta_pacbio_library.merge(well2bc, on='well_pacbio')

picogreen = pd.read_csv('../../metadata/Picogreen_PacBio_Exp1.csv')
picogreen = picogreen[['Orig well','corrected','initial.10x.dil.']]
picogreen.rename(columns={'Orig well': 'well_pacbio','corrected': 'cDNA_concentration','initial.10x.dil.': 'initial_10x_dil_library'}, inplace=True)

meta_pacbio_library = meta_pacbio_library.merge(picogreen, on='well_pacbio')

metadata = meta_pacbio_library.merge(qc_cell, left_on='bc_id',right_index=True)
metadata= metadata.merge(meta_shortread, left_on='Cellname', right_index=True)

metadata.to_csv('../../metadata/metadata_all_pb_exp1.csv', index=False)


# Make metadata for genes and isoforms
metadata = pd.read_csv('../../metadata/metadata_all_pb_exp1.csv')
# Filter out low-quality cells

bcs_use = metadata.bc_id[metadata.nReads > 1000]
bcs_use

data.shape
data_filtered = data.loc[data.bc_id.isin(bcs_use),]
data_filtered.shape

# Isoforms
nCells_isoform=pd.DataFrame(data_filtered[['pbid','bc_id','gene','category']].drop_duplicates().groupby(['pbid','gene','category']).size())
nCells_isoform.columns=['nCells']
nCells_isoform.reset_index(inplace=True)
#nCells_isoform.head()

nReads_isoform=pd.DataFrame(data_filtered.groupby(['pbid']).size())
nReads_isoform.columns=['nReads']
nReads_isoform.reset_index(inplace=True)
#nReads_isoform.head()

metadata_isoform = nCells_isoform.merge(nReads_isoform, on='pbid')
#metadata_isoform.head()

# Get rank of each isoform within each gene - first by nCells/isoform, then break nCell ties by nReads

metadata_isoform=metadata_isoform.set_index('pbid')
isoform_rank = pd.DataFrame(metadata_isoform.sort_values(['gene','nCells','nReads'], ascending=False).groupby('gene').cumcount())
isoform_rank.columns=['isoform_rank']
#isoform_rank.head()
metadata_isoform=metadata_isoform.merge(isoform_rank, left_index=True, right_index=True)
#metadata_isoform.loc[metadata_isoform.gene.str.contains('Nrxn3'),].sort_values(['gene','nCells','nReads'],ascending=False)
metadata_isoform.to_csv('../../metadata/isoform_metadata.csv')


# Genes
nCells_gene=pd.DataFrame(data_filtered[['bc_id','gene']].drop_duplicates().groupby(['gene']).size())
nCells_gene.columns=['nCells']
nCells_gene.reset_index(inplace=True)
#nCells_gene.head()

nReads_gene=pd.DataFrame(data_filtered.groupby(['gene']).size())
nReads_gene.columns=['nReads']
nReads_gene.reset_index(inplace=True)
#nReads_gene.head()

metadata_gene= nCells_gene.merge(nReads_gene, on='gene').set_index('gene')

#metadata_gene.head()

metadata_gene.to_csv('../../metadata/gene_metadata.csv')
