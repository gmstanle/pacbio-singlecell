#!/bin/bash
#SBATCH --job-name=STAR_generate_genome
#SBATCH --ntasks=1
#SBATCH -N 1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --partition=normal,owners,quake

# Generates genome index for STAR

SJDB_OVERHANG=49 # ReadLength - 1; 100 is a good default.

#STAR=/local10G/resources/STAR-STAR_2.4.2a/bin/Linux_x86_64/STAR
ml load biology
ml load star

date
echo "Starting STAR to generate genome..."

#####
# For generating a standard genome
GENOME_DIR=/oak/stanford/groups/quake/gmstanle/references/pacbio-singlecell/bcPCRv2_GRCh38
GENOME_FASTA_FILES=/oak/stanford/groups/quake/gmstanle/references/pacbio-singlecell/bcPCRv2_GRCh38/Homo_sapiens.GRCh38.82.dna.primary_assembly.ERCC.bcPCRv2.fa
SJDB_GTF_FILE=/oak/stanford/groups/quake/gmstanle/references/pacbio-singlecell/bcPCRv2_GRCh38/Homo_sapiens.GRCh38.82.ERCC.bcPCRv2.gtf

STAR --runMode genomeGenerate --genomeDir $GENOME_DIR --genomeFastaFiles $GENOME_FASTA_FILES --sjdbGTFfile $SJDB_GTF_FILE --sjdbOverhang $SJDB_OVERHANG --genomeSAsparseD 2 --runThreadN 16 > $GENOME_DIR/genomeGenerate.log
date
echo "Done!!"
