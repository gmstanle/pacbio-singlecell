# Script to count the PacBio barcodes produced by STAR alignment of cells sequenced with Illumina that have bcPCRv2 barcodes
# 190530 Geoff Stanley

import pandas as pd
import numpy as np
import glob


bc_counts_all = pd.DataFrame(columns=['count','cell','bc_well'])
#print(bc_counts)

CHIMERA_DIR = '../../../../../storage/pacbio-singlecell/190502_bcPCRv2_TuPa23_exp1/'
chimera_files = glob.glob(CHIMERA_DIR + '*Chimeric.out.junction')
# print(chimera_files)

# iterate through chimeric junction files (1 file = 1 demuxed Nextera barcode)
for f in chimera_files:
    junctions = pd.read_csv(f,header=None,  sep='\t')
    cell_name = "_".join(f.split('/')[-1].split('_')[0:4])
    expected_barcode = cell_name.split("_")[1]
     
    # print(junctions[1].head())
    bc_regexp = "CS[12]_bc10"

    # get the indexes of the junctions containing the PB barcode on either one side or another
    mask_0 = junctions[0].str.contains(bc_regexp) # reads that map to barcodes on left side
    mask_1 = junctions[3].str.contains(bc_regexp) # reads that map to barcodes on right side


    if(np.sum(mask_0) + np.sum(mask_1) == 0 ): # if there are no PB barcode-mapping reads
        bc_counts = pd.DataFrame({'count': [0], 'cell': [cell_name], 'bc_well': [expected_barcode]})
    else:
        barcodes_0  = junctions[mask_0] 
        barcodes_1  = junctions[mask_1]

        # get the "well" identifier of the PacBio barcode - i.e., which well of the 96-well primer plate did it come from
        if np.sum(mask_0)==0:
            bc_wells =  barcodes_1[3].str.split("_", expand=True)[0].tolist()
        elif np.sum(mask_1)==0:
            bc_wells =  barcodes_0[0].str.split("_", expand=True)[0].tolist() 
        else:
            bc_wells =  barcodes_0[0].str.split("_", expand=True)[0].tolist() + barcodes_1[3].str.split("_", expand=True)[0].tolist()


        # print(barcodes_0.tail())
      
        # barcodes_1[3].str.split("_")[3].tolist()
        # print(bc_wells[1:10])
        # print(np.sum(mask_0) + np.sum(mask_1))
        # print(len(bc_wells))

        # Count the number of reads per barcode
        bc_counts = pd.DataFrame(np.unique(np.asarray(bc_wells), return_counts=True))
        bc_counts.columns = bc_counts.iloc[0].tolist()
        bc_counts = pd.DataFrame(bc_counts.iloc[1])
        bc_counts.columns = ['count']
        # print(bc_counts)

        # Append to output dataframe
        bc_counts['cell'] = cell_name
        bc_counts['bc_well'] = bc_counts.index
        bc_counts.reset_index(drop=True, inplace=True)
        # print(bc_counts)
        bc_counts_all = bc_counts_all.append(bc_counts)
        # print(cell_name)
        # print(f)
        # print(junctions.head())

# print(bc_counts_all.head())
# print(bc_counts_all.tail())
# print(bc_counts_all.shape)

# Pivot the barcode reads to give a matrix of barcode index ~ cell (where cell = Nextera index)
bc_count_mat = bc_counts_all.pivot(index = 'bc_well', columns = 'cell')
bc_count_mat.fillna(value=0, inplace=True)
# print(bc_count_mat)
bc_count_mat.to_csv('bc_counts.txt', sep='\t')




