# Script to munge results from nf-core-scisoseq: filter invalid barcodes, and assign barcode combinations to cells
# according to my barcode scheme. The count table has isoform id as rows and barcodes as columns.
# Usage: python make_raw_data.py <results dir> <experiment name> <output dir>
# <results_dir> is the directory containing the output of the nf-core-scisoseq pipeline

import sys
import os
from dotenv import load_dotenv, find_dotenv
import pandas as pd
import numpy as np

# find .env automagically by walking up directories until it's found
# load up the entries as environment variables
dotenv_path = find_dotenv()
load_dotenv(dotenv_path)

results_dir = os.environ.get("SCISOSEQ_RESULTS_DIR")
expt_name = os.environ.get("EXPERIMENT_NAME")
out_dir   = "../../data/"

# raw data post SQANTI filter
raw_data_file = os.path.join(results_dir, 'collate_results') 
raw_data_file = os.path.join(raw_data_file, expt_name + '.trimmed.annotated.csv')
raw_data = pd.read_csv(raw_data_file,header=0,sep='\t', index_col='id')
print(raw_data.head())
print('Number of input reads: ' + str(raw_data.shape[0]))

raw_data.insert(0,'First_bc_num',raw_data['BC_1'].str.slice(4, 10))
raw_data.insert(0,'Combined_bc_num',raw_data['BC_2'].str.slice(4, 10))

def determine_valid(bc1, bc2):
    if(bc1==bc2):
        return('valid')
    else:
        return('invalid')

is_valid = raw_data['First_bc_num'] == raw_data['Combined_bc_num']
raw_data.insert(0,'is_valid_bc', is_valid)
raw_data = raw_data.loc[raw_data['is_valid_bc'], ]
print('Number of reads with valid bc pairs: ' + str(raw_data.shape[0]))

raw_data.insert(0, 'bc_id', raw_data['First_bc_num'])
raw_data.drop(columns=['First_bc_num','Combined_bc_num','is_valid_bc'])

out_file = os.path.join(out_dir, expt_name + '.raw_data.csv')
raw_data.to_csv(out_file)

count_table = raw_data.groupby(by=['pbid','bc_id']).size().unstack(fill_value=0)

count_file = os.path.join(out_dir, expt_name + '.isoform_counts.tsv')
count_table.to_csv(count_file, sep='\t')

# raw data with no SQANTI filter
raw_data_file = os.path.join(results_dir, 'collate_results') 
raw_data_file = os.path.join(raw_data_file, expt_name + '.trimmed.annotated.no_sqanti_filter.csv')
raw_data = pd.read_csv(raw_data_file,header=0,sep='\t', index_col='id')
print(raw_data.head())
print('Number of input reads: ' + str(raw_data.shape[0]))

raw_data.insert(0,'First_bc_num',raw_data['BC_1'].str.slice(4, 10))
raw_data.insert(0,'Combined_bc_num',raw_data['BC_2'].str.slice(4, 10))

def determine_valid(bc1, bc2):
    if(bc1==bc2):
        return('valid')
    else:
        return('invalid')

is_valid = raw_data['First_bc_num'] == raw_data['Combined_bc_num']
raw_data.insert(0,'is_valid_bc', is_valid)
raw_data = raw_data.loc[raw_data['is_valid_bc'], ]
print('Number of reads with valid bc pairs, no SQANTI filter: ' + str(raw_data.shape[0]))

raw_data.insert(0, 'bc_id', raw_data['First_bc_num'])
raw_data.drop(columns=['First_bc_num','Combined_bc_num','is_valid_bc'])

out_file = os.path.join(out_dir, expt_name + '.raw_data.no_sqanti_filter.csv')
raw_data.to_csv(out_file)
