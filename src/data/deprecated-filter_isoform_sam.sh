#!/bin/bash
# filter the .sam of collapsed isoforms from the sqanti_qc step of nf-core-scisoseq
# usage: sh filter_isoformSAM_byRead.sh <list of isoforms to select> <scisoseq results directory> <experiment name> <output name>

# This script is *really* slow. Going to switch to using Pandas dataframes to load for now 

# This is the input of corrected and collapsed isoforms output by the SQANTI_QC step
INPUT_FNAME=$3".collapsed.rep.renamed_corrected.sam"
INPUT=$2"/sqanti_qc/"$INPUT_FNAME

echo "list of isoforms: " $1 
echo "input: " $INPUT
echo "experiment name: " $3
echo "output file: " $4

sed 's/$/\\s/g' $1 > tmp.txt
sed -i 's/^/^/g' tmp.txt
samtools view -HS $INPUT > $4
grep -f tmp.txt $INPUT >> $4
# tmp.txt is stored in the directory where the script is called from (e.g. the notebook directory if it's called from a notebook)
#rm tmp.txt