# Requires that the make_raw_data.py, make_all_metadata.py scripts have been run already
# Filters the .bam file produced by the number of cells an isoform is detected in

import pandas as pd
import HTSeq as ht
import sys

MIN_CELLS = 3

ALIGNMENT_FILE = '/storage/pacbio-singlecell/pacbio_exp1/align_results_200330/pacbio_exp1.ccs/sqanti_qc/pacbio_exp1.ccs.collapsed.rep.renamed_corrected.sam'

isoform_metadata = pd.read_csv('../../metadata/isoform_metadata.csv', index_col=0)
print(isoform_metadata.shape)
pass_isoforms = isoform_metadata.index[isoform_metadata.nCells >= MIN_CELLS]
print(pass_isoforms.shape)

