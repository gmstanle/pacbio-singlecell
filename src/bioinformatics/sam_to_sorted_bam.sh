#!/bin/bash
# takes a sam file, outputs as bam, sorts the bam, and indexes the bam. 
# This should be run in the same directory as the input file
# Usage: bash sam_to_sorted_bam.sh <input sam filename (not path)> 

# remove the .sam to get the prefix
PREFIX=$1
PREFIX=${PREFIX%%'.sam'}
samtools view -bS $1 > $PREFIX'.bam'
samtools sort -@4 -m 4G $PREFIX'.bam' $PREFIX'.sorted'
samtools index $PREFIX'.sorted.bam'
rm $PREFIX'.bam'
