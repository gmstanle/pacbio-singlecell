#!/bin/sh
set -eu

FASTQ_DIR=/oak/stanford/groups/quake/gmstanle/sequencing-data/pacbio-singlecell/190502_bcPCRv2_TuPa23_exp1/fastqs
OUTPUT_DIR=/oak/stanford/groups/quake/gmstanle/sequencing-data/pacbio-singlecell/190502_bcPCRv2_TuPa23_exp1/star-output

echo "Output directory $OUTPUT_DIR"
for F in $FASTQ_DIR/*_R1_*.fastq.gz ; do
  # TODO add an if STAR output already exists, skip
  R1=`echo $F | sed s/_R2_/_R1_/`
  R2=`echo $F | sed s/_R1_/_R2_/`
  echo "Processing $R1 $R2"
  ./star-wrapper.sh $R1 $R2 $OUTPUT_DIR
  echo "Done"
done
