#!/bin/sh
set -eu

## this is a wrapper for mapping paired end data
# $1 is fastq for read 1, $2 is fastq for read 2, and $3 is output directory
# requires 64 GB RAM due to 20GB sam sort memory and compressed genome which is kept in mem
# adapted from https://www.biostars.org/p/267075/
module load biology
module load star

DBDIR=/oak/stanford/groups/quake/gmstanle/references/pacbio-singlecell/bcPCRv2_GRCh38

OUTDIR=$3
BASE=`basename $1`
ZCAT="--readFilesCommand zcat"
THREADS=16
OPTS_P1="--outSAMunmapped Within --outSAMstrandField intronMotif --outSAMtype BAM SortedByCoordinate --limitBAMsortRAM 20000000000 --genomeLoad LoadAndKeep --outFileNamePrefix $OUTDIR/${BASE} --quantMode GeneCounts --chimSegmentMin 15"

if [ "$#" -ge 4  ]; then
   echo "illegal number of parameters"
   exit 1
fi

if [ "$#" -eq 3 ]; then
CMD="STAR --runThreadN $THREADS --outBAMsortingThreadN 16 $OPTS_P1 $ZCAT  --genomeDir $DBDIR --readFilesIn $1 $2"
echo $CMD
$CMD
fi
